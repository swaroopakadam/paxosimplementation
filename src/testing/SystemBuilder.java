package testing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import helper.Participant;
import helper.PaxosEvent;

public class SystemBuilder {
	private static int nodes;
	private static HashMap<Integer,String>proposerMap=new HashMap<Integer,String>();
	private static ArrayList<Participant> nodesList=new ArrayList<Participant>();
	private static String consensus;
	private static String consensusValue;
	private static int requestId;
	private static void collectDataFromUser(){
		Scanner in = new Scanner(System.in);
		System.out.println("How many nodes in the system?");
		nodes=in.nextInt();
		System.out.println("Has consesus been established?");
		consensus=in.next();
		if(consensus.equalsIgnoreCase("Yes")){
			System.out.println("What is the prepare request id in the system?");
			requestId = in.nextInt();
			System.out.println("What is the consensus value in the system?");
			consensusValue=in.next();
			
		}
		System.out.println("How many nodes propose the value?");
		int numProposers=in.nextInt();
		for(int i=0;i<numProposers;i++){
			System.out.println("node number and value proposed");
			proposerMap.put(in.nextInt(),in.next());	
		}
	}

	private static void initializeSystem() {
		String pValue=null;
		
		for(int i =0;i<nodes;i++){
			if(proposerMap.containsKey(i+1)){
				pValue=proposerMap.get(i+1);
			}
			Participant participant=new Participant(i+1,nodes,pValue);
			
			nodesList.add(participant);
			pValue=null;
			
		}
		PaxosEvent pe= new PaxosEvent(nodesList,proposerMap,consensusValue,requestId);
				
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		collectDataFromUser();
		initializeSystem();

		
		
	}

}
