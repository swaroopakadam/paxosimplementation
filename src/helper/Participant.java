package helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import constants.PaxosMessageType.messageType;

public class Participant implements Runnable{
	int nodeId;
	Thread t;
	
	PaxosMessage paxosMessage;
	String learnedValue=null;
	String acceptedValue=null;
	String defaultValue=null;
	boolean learned=false;
	int totalNodes;	
	int prepareRequestId=-1;
	
	public int getPrepareRequestId() {
		return prepareRequestId;
	}
	public void setPrepareRequestId(int prepareRequestId) {
		this.prepareRequestId = prepareRequestId;
	}
	public int getNodeId() {
		return nodeId;
	}
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}
	public Participant(int i,int totalNodes, String pValue) {
		// TODO Auto-generated constructor stub
		nodeId=i;
		this.totalNodes=totalNodes;
		this.defaultValue=pValue;
	}
	@Override
	public void run() {

		ArrayList<PaxosMessage> responsesP1=new ArrayList<PaxosMessage>();
		ArrayList<String>majorityResponsesP1= new ArrayList<String>();
		ArrayList<PaxosMessage> responsesP2=new ArrayList<PaxosMessage>();
		while(learned==false){
			int receiverId=PaxosMessageQueueManager.getRecieverId();
			if(receiverId==-1){
				//System.out.println("No message for "+nodeId);
			}else if(nodeId==receiverId){
				try {
					Thread.sleep((long)((new Random()).nextInt(5) * 1000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				paxosMessage = PaxosMessageQueueManager.dequeue();
				messageType pmt=paxosMessage.getMessageType();
				
				switch (pmt) {
				case PrepareRequest:
					System.out.println("running for : "+nodeId + " PrepareRequest");
					System.out.println(prepareRequestId + " and " +paxosMessage.getPrepareRequestId());

					PaxosMessage responsePaxosMessage=null;
					if(paxosMessage.getPrepareRequestId() < prepareRequestId){
						responsePaxosMessage= new PaxosMessage(nodeId, paxosMessage.getSenderId(),learnedValue , messageType.Reject);
					}else{
						if(learnedValue==null){
							
							responsePaxosMessage = new PaxosMessage(nodeId,paxosMessage.getSenderId() , acceptedValue, messageType.ResponseToPrepreRequest);
							
						}else if (paxosMessage.getPrepareRequestId()>prepareRequestId){
							responsePaxosMessage = new PaxosMessage(nodeId,paxosMessage.getSenderId() , null, messageType.ResponseToPrepreRequest);
							
						}else {
							responsePaxosMessage = new PaxosMessage(nodeId,paxosMessage.getSenderId() , learnedValue, messageType.ResponseToPrepreRequest);
						}
						prepareRequestId=paxosMessage.getPrepareRequestId();
					}
					responsePaxosMessage.setPrepareRequestId(prepareRequestId);

					PaxosMessageQueueManager.enqueue(responsePaxosMessage);

					break;
				case ResponseToPrepreRequest:
					System.out.println("running for : "+nodeId + " ResponseToPrepreRequest");
					System.out.println("senderId "+ paxosMessage.getSenderId()+"receiverId:" +paxosMessage.getreceiverId()+" value : "+paxosMessage.getValue());

					responsesP1.add(paxosMessage);
					String value=defaultValue;
					
					if(responsesP1.size()>=(totalNodes/2)+1){
						for(PaxosMessage pm:responsesP1){
							if(pm.getValue()!=null){
								majorityResponsesP1.add(pm.getValue());
							}
						}
						Collections.sort(majorityResponsesP1);
						String majority = null;
						int count=0;
						for(String s:majorityResponsesP1){
							if(count==0){
								majority=s;
							}
							if(s.equals(majority)){
								count++;
							}else{
								count--;
							}
						}
						//System.out.println(majority+" " +count+" "+ majorityResponsesP1.size());
						if(majority!=null && count>=majorityResponsesP1.size()/2){
							value=majority;
						}
						for(int i=0 ;i<totalNodes;i++){
							PaxosMessage pm= new PaxosMessage(nodeId, i+1, value, messageType.AcceptRequest);
							PaxosMessageQueueManager.enqueue(pm);
						}
						
					}
					
					
					break;
				case AcceptRequest:
					//set the value to accepted value 
					acceptedValue=paxosMessage.getValue();
					System.out.println("running for : "+nodeId + " AcceptRequest");
					System.out.println("senderId "+ paxosMessage.getSenderId()+"receiverId:" +paxosMessage.getreceiverId()+" value : "+paxosMessage.getValue());

					//send the message to all the nodes with MessageType = DecisionProposal
					for(int i=0 ;i<totalNodes;i++){
						PaxosMessage pm= new PaxosMessage(nodeId, i+1, acceptedValue, messageType.DecisionProposal);
						PaxosMessageQueueManager.enqueue(pm);
					}
					
					break;
				case DecisionProposal:
					System.out.println("running for : "+nodeId + " DecisionProposal");
					System.out.println("senderId "+ paxosMessage.getSenderId()+"receiverId:" +paxosMessage.getreceiverId()+" value : "+paxosMessage.getValue());

					//count number decision proposals received 
					//if mojority 
					//set the learned Value to value in the message
					responsesP2.add(paxosMessage);
					value=paxosMessage.getValue();
					if(responsesP2.size()>=(totalNodes/2)){
						learnedValue=value;
						learned=true;
						//prepareRequestId=paxosMessage.getPrepareRequestId();
						System.out.println("Node Id "+nodeId+" has learned the value "+learnedValue);
					}
					//t.stop();
					break;
				case Reject:
					acceptedValue=paxosMessage.getValue();
					System.out.println("running for : "+nodeId + " AcceptRequest");
					System.out.println("senderId "+ paxosMessage.getSenderId()+"receiverId:" +paxosMessage.getreceiverId()+" value : "+paxosMessage.getValue());

					//send the message to all the nodes with MessageType = DecisionProposal
					for(int i=0 ;i<totalNodes;i++){
						PaxosMessage pm= new PaxosMessage(nodeId, i+1, acceptedValue, messageType.DecisionProposal);
						PaxosMessageQueueManager.enqueue(pm);
					}
					break;
				default:
					break;
				}
			}
		}
		
	}
	public String getLearnedValue() {
		return learnedValue;
	}
	public void setLearnedValue(String learnedValue) {
		this.learnedValue = learnedValue;
	}
	public String getAcceptedValue() {
		return acceptedValue;
	}
	public void setAcceptedValue(String acceptedValue) {
		this.acceptedValue = acceptedValue;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public void start () {
	      System.out.println("Starting " +  nodeId );
	      if (t == null) {
	         t = new Thread (this);
	         t.start ();
	      }
	   }
}
