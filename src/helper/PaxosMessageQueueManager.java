package helper;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class PaxosMessageQueueManager {
	private static BlockingQueue<PaxosMessage> channel = new LinkedBlockingQueue<PaxosMessage>();
	
	public static int getRecieverId() {
		if(!channel.isEmpty()){
			PaxosMessage pMessage= channel.peek();
			if(pMessage==null){
				return -1;
			}
			return pMessage.getreceiverId();
		}
		return -1;
		
	}
	public static PaxosMessage dequeue() {
		if(channel.isEmpty()){
			return null;
		}
		else {
			try {
				return channel.take();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
		
	}
	public static void enqueue(PaxosMessage pm) {
		// TODO Auto-generated method stub
		channel.add(pm);
		
	}
	@SuppressWarnings("unchecked")
	public static void shuffle() {
		// TODO Auto-generated method stub
		Collections.shuffle((List<PaxosMessage>) channel);
		
	}
}
