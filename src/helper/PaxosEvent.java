package helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import constants.PaxosMessageType.*;

public class PaxosEvent {
	private String value;

	public PaxosEvent(ArrayList<Participant> nodesList, HashMap<Integer, String> proposerMap,String consensusValue, int requestId) {

		PaxosMessage[] pm = new PaxosMessage[nodesList.size()];
		HashMap<Integer, Integer> proposeRequestMap = new HashMap<Integer, Integer>();

		for (int i = 0; i < nodesList.size(); i++) {
			int receiverId = nodesList.get(i).nodeId;
			nodesList.get(i).start();
			if(consensusValue!=null){
				nodesList.get(i).setLearnedValue(consensusValue);
				nodesList.get(i).setPrepareRequestId(requestId);
			}
			
			for (int senderId : proposerMap.keySet()) {
				if (!proposeRequestMap.containsKey(senderId)) {
					proposeRequestMap.put(senderId, getPrepareRequestId());
					//System.out.println("Node "+senderId+" with proposer request number : "+getPrepareRequestId());

				}
				int prepareRequestId = proposeRequestMap.get(senderId);
				pm[i] = new PaxosMessage(senderId, receiverId, proposerMap.get(senderId), messageType.PrepareRequest);
				
				pm[i].setPrepareRequestId(prepareRequestId);
				
				PaxosMessageQueueManager.enqueue(pm[i]);
			}
		}
	}

	private int getPrepareRequestId() {
		HashSet<Integer> prepareRequestIdSet = new HashSet<Integer>();
		Random random = new Random();
		int maxInSet = -1;
		int e;

		for (int d : prepareRequestIdSet) {
			if (d > maxInSet) {
				maxInSet = d;
			}
		}

		do {
			e = random.nextInt(10)+1;
		} while (prepareRequestIdSet.contains(e) && e < maxInSet);
		prepareRequestIdSet.add(e);
		return e;

	}

	public String getLearnedValue() {

		return value;
	}

}
