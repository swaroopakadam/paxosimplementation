package helper;

import java.util.HashMap;

import constants.PaxosMessageType.messageType;;

public class PaxosMessage {
	
	private messageType mType;
	private HashMap<Integer, String>proposerMap=new HashMap<Integer,String>();
	private int receiverId;
	private String value;
	private int prepareRequestId;
	private int senderId;
	
	
	public messageType getMessageType() {
		return mType;
	}


	public void setMessageType(messageType messageType) {
		this.mType = messageType;
	}


	public int getSenderId() {
		return senderId;
	}


	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}


	public int getreceiverId() {
		return receiverId;
	}


	public void setreceiverId(int receiverId) {
		this.receiverId = receiverId;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public int getPrepareRequestId() {
		return prepareRequestId;
	}


	public void setPrepareRequestId(int prepareRequestId) {
		this.prepareRequestId = prepareRequestId;
	}


	public PaxosMessage(HashMap<Integer, String> proposerMap, int receiverId, messageType mType) {
		// TODO Auto-generated constructor stub
		this.proposerMap=proposerMap;
		this.receiverId=receiverId;
		this.mType=mType;
	}


	public PaxosMessage(int senderId, int receiverId, String value, messageType mType) {
		// TODO Auto-generated constructor stub
		this.senderId=senderId;
		this.receiverId=receiverId;
		this.value=value;
		this.mType=mType;
	}
	
	

}
